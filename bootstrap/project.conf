name: base-sdk-bootstrap

aliases:
  flathub: https://flathub.org/

element-path: elements

variables:
  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  sbindir: "%{bindir}"
  sysconfdir: "%{prefix}/etc"
  localstatedir: "%{prefix}/var"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  sysroot: /cross-installation
  cross-install: make -j1 install DESTDIR="%{install-root}%{sysroot}"
  tools: /cross
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{target_arch}-unknown-linux-%{abi}"
  gcc_arch: "%{target_arch}"
  abi: "gnu"
  (?):
  - target_arch == "i586":
      gcc_arch: "i386"
  - target_arch == "arm":
      abi: "gnueabihf"

  strip-binaries: |
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
      -exec sh -ec \
      'read -n4 hdr <"$1" # check for elf header
       if [ "$hdr" != "$(printf \\x7fELF)" ]; then
           exit 0
       fi
       case "$1" in
         "%{install-root}%{debugdir}/"*|"%{install-root}%{sysroot}%{debugdir}/"*)
           exit 0
           ;;
         "%{install-root}%{sysroot}/"*)
           realpath="$(realpath -s --relative-to="%{install-root}%{sysroot}" "$1")"
           debugfile="%{install-root}%{sysroot}%{debugdir}/${realpath}.debug"
           toolchain=%{triplet}-
           ;;
         *)
           realpath="$(realpath -s --relative-to="%{install-root}" "$1")"
           debugfile="%{install-root}%{debugdir}/${realpath}.debug"
           ;;
       esac
       if "${toolchain}objdump" -j .gnu_debuglink -s "$1" &>/dev/null; then
         exit 0
       fi
       mkdir -p "$(dirname "$debugfile")"
       "${toolchain}objcopy" %{objcopy-extract-args} "$1" "$debugfile"
       chmod 644 "$debugfile"
       "${toolchain}strip" %{strip-args} "$1"
       "${toolchain}objcopy" %{objcopy-link-args} "$debugfile" "$1"' - {} ';'

environment:
  FORCE_UNSAFE_CONFIGURE: "1"
  PATH: "%{tools}/bin:/bin"
  CPPFLAGS:  "-O2 -D_FORTIFY_SOURCE=2"
  CFLAGS: "-O2 -g -fstack-protector-strong"
  CXXFLAGS: "-O2 -g -fstack-protector-strong"
  LDFLAGS: "-fstack-protector-strong -Wl,-z,relro,-z,now"

split-rules:
  devel:
    - "%{sysroot}%{includedir}"
    - "%{sysroot}%{includedir}/**"
    - "%{sysroot}%{libdir}/pkgconfig"
    - "%{sysroot}%{libdir}/pkgconfig/**"
    - "%{sysroot}%{datadir}/pkgconfig"
    - "%{sysroot}%{datadir}/pkgconfig/**"
    - "%{sysroot}%{datadir}/aclocal"
    - "%{sysroot}%{datadir}/aclocal/**"
    - "%{sysroot}%{prefix}/lib/cmake"
    - "%{sysroot}%{prefix}/lib/cmake/**"
    - "%{sysroot}%{libdir}/cmake"
    - "%{sysroot}%{libdir}/cmake/**"
    - "%{sysroot}%{prefix}/lib/lib*.a"
    - "%{sysroot}%{libdir}/lib*.a"
    - "%{sysroot}%{prefix}/lib/lib*.la"
    - "%{sysroot}%{libdir}/*.la"

options:
  target_arch:
    type: arch
    description: Target architecture
    variable: target_arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

  build_arch:
    type: arch
    description: Build architecture
    variable: build_arch
    values:
      - arm
      - aarch64
      - i586
      - x86_64

artifacts:
  url: https://artifacts.freedesktop.codethink.co.uk/artifacts/

elements:
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}
